
# Databox challenge task
****
Service forwards data from:
 
- Youtube channel with ID *UC7_gcs09iThXybpVgjHZ_7g*
- FakeJsonAPI provider which generates some data for marketing

to databox endpoint, registered for user "*zan@majeric.si*".

#### Running and deployment

- Run ```composer install```
- Then start the server with ``php artisan serve`` (optionally add other parameters such as host,
port,...)

#### Service endpoints

- ``/api/forward/all`` forwards both providers data

*Fake JSON API*
- ``/forward/fakedata/pushSellersOfTheDay``  Pushes sellers of the day
- ``/forward/fakedata/pushSellProgress/{units}``  Pushes sell progress, number of units as parameter
- ``/forward/fakedata/pushProductsUnitsSold`` Pushes sold products
- ``/forward/fakedata/pushSellersOfTheDay``  Pushes sellers of the day from FakeJSON API

*Youtube API*
- ``/forward/youtube/pushGeneralData/``  Pushes general data for channel
- ``/forward/youtube/pushMostViewed/``  Pushes most viewed videos of channel

#### Console

You can also send data from **artisan** with following command:
``artisan send:metrics`` use ``--help`` for further information.

#### Periodic sending

Configure laravel scheduling functionality with following command: ``artisan schedule:run``.
This will execute pushing from both APIs every minute. Configure that in 
``App\Console\Kernel::schedule()`` method. You'll also have to add cronjobs to your [server 
setup](https://laravel.com/docs/5.6/scheduling#introduction).
