var elixir = require('laravel-elixir');

elixir(function(mix) {
    mix.sass('app.scss');
    mix.scripts(['app.js', /*'controllers.js'*/], 'public/js/app.js')
    /*.scripts(['forum.js', 'threads.js'], 'public/js/forum.js')*/;
});