<?php

namespace App\Providers;

use App\Http\YoutubeRetriever;
use App\Services\FakeJsonPusher;
use App\Services\YoutubePusher;
use Databox\Client as Databox;
use Google_Client as GClient;
use Google_Service_Youtube as YTService;
use Illuminate\Support\ServiceProvider;

class ApisServiceProvider extends ServiceProvider {
    protected $defer = true;

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot() {
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register() {
        $this->app->singleton('Databox\Client', function ($app) {
            return new Databox("hy3i22ixenmsuoda3fs7xp");
        });
        $this->app->singleton('Google_Service_Youtube', function ($app) {
            $client = new GClient();
            $client->setAuthConfig(base_path('') . '\google_credentials.json');
            $client->setDeveloperKey('3345cdc6baa730b5fad775c4825c7e102db3c237');
            $client->setScopes(\Google_Service_YouTube::YOUTUBE);
            $client->setAccessType('offline');

            if ($client->fetchAccessTokenWithAssertion()) {
                $youtubeReporting = new YTService($client);
            } else
                throw new \Exception("YoutubeClientException: Error while fetching token with assertion!");
            return $youtubeReporting;
        });
        $this->app->singleton('FakeJsonPusher',function($app){
            new FakeJsonPusher();
        });
        $this->app->singleton('YoutubePusher',function($app){
            new YoutubePusher();
        });
        $this->app->tag(['FakeJsonPusher', 'YoutubePusher'], 'pushers');
        $this->app->singleton('Pushers', function ($app) {
            phpinfo();
        });
    }

    public function provides() {
        return ['MyDatabox\Client','Google_Client','FakeJsonPusher','YoutubePusher'];
    }
}
