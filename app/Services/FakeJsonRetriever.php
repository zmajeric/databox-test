<?php
/**
 * Created by PhpStorm.
 * User: zmajeric
 * Date: 04/07/2018
 * Time: 07:01
 */

namespace App\Services;

use GuzzleHttp;

class FakeJsonRetriever {

    const ENDPOINT = 'http://app.fakejson.com/q';
    const TOKEN = 'VNduBBYZRnxFQ3rsFdoCpA';

    private $client;

    public function __construct() {
        $header = array('content-type' => 'application/json');
        $this->client = new GuzzleHttp\Client([
            'headers' => $header
        ]);
    }

    public function getFakeNumbers($count): array {
        $params = array(
            'token' => self::TOKEN,
            'parameters' => array(
                'code' => "404",
                'delay' => "5",
                'consistent' => false
            ),
            'data' => array(
                "numberInt" => "numberInt",
                "numberFloat" => "numberFloat",
                "numberBool" => "numberBool",
                "_repeat" => $count
            )
        );
//        $response = $this->request($params);
//        return $response;
    }

    public function getFakeNames($count): array {
        $params = array(
            'token' => self::TOKEN,
            'parameters' => array(
                'code' => "404",
                'delay' => "5",
                'consistent' => false
            ),
            'data' => array(
                "nameFirst" => "nameFirst",
                "nameLast" => "nameLast",
                "_repeat" => $count
            )
        );

//        $response = $this->request($params);
        $response = json_decode('[{"nameLast":"Beatty","nameFirst":"Pinkie"},{"nameLast":"Toy","nameFirst":"Marcella"},{"nameLast":"Lowe","nameFirst":"Ali"},{"nameLast":"Murray","nameFirst":"Timmy"},{"nameLast":"Ritchie","nameFirst":"Miles"}]');
        return $response;
    }

    public function getFakeProducts($count) {
        $params = array(
            'token' => self::TOKEN,
            'parameters' => array(
                'code' => "404",
                'delay' => "5",
                'consistent' => false
            ),
            'data' => array(
                "productName" => "productName",
                "productSize" => "productSize",
                "productOrderStatus" => "productOrderStatus",
                "_repeat" => $count
            )
        );
//        $response = $this->request($params);
        $response = json_decode('[{"productSize":"XS","productOrderStatus":"in-progress","productName":"Quad sunron"},{"productSize":"S","productOrderStatus":"started","productName":"Kayremtom"},{"productSize":"L","productOrderStatus":"cancelled","productName":"Salt-dam"},{"productSize":"M","productOrderStatus":"completed","productName":"Zonehold"},{"productSize":"XL","productOrderStatus":"started","productName":"Tech jaytop"},{"productSize":"2XL","productOrderStatus":"in-progress","productName":"Latoveing"},{"productSize":"2XS","productOrderStatus":"completed","productName":"Solflex"},{"productSize":"3XL","productOrderStatus":"cancelled","productName":"Gravetough"},{"productSize":"S","productOrderStatus":"cancelled","productName":"Tamp-san"},{"productSize":"XS","productOrderStatus":"completed","productName":"Trans-ap"}]');

        return $response;
    }

    public function request($data) {
        $data = json_encode($data);
        return json_decode($this->client->post(self::ENDPOINT, [
                'body' => $data
            ]
        )->getBody()->getContents());
    }

}