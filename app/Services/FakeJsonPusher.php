<?php
/**
 * Created by PhpStorm.
 * User: zmajeric
 * Date: 04/07/2018
 * Time: 09:07
 */

namespace App\Services;

class FakeJsonPusher extends Pusher {

    const DATABOX_MARKET_TOKEN = '60rb9x1s7sxselsifa8zy';
    const CURRENCY_UNIT = 'EUR';

    private $databoxClient;
    private $retriever;
    private $date;

    public function __construct() {
        parent::__construct(self::DATABOX_MARKET_TOKEN);
        $this->retriever = new FakeJsonRetriever();
        $this->date = now()->toDateTimeString();
    }

    /**
     * @zmajeric ~ 04/07/2018
     *
     * Pushes sellers of the day to Databox.
     *
     * @return array
     */
    public function pushSellersOfTheDay() {
        $names = $this->retriever->getFakeNames(5);
//        $dummyValues = $this->retriever->getFakeNumbers(5);

        $metrics = array();
        foreach ($names as $id => $name) {
            // TODO: change rand to actual values from API
            $metrics[] = ['SellersOfTheDay', rand(0, 300), $this->date,
                ['channel' => $name->nameLast . ' ' . $name->nameFirst], self::CURRENCY_UNIT];
        }

        return parent::insert($metrics);
    }

    /**
     * @zmajeric ~ 04/07/2018
     *
     * Pushes sell progress with a amount of sold Products.
     *
     * @param $sold
     * @return array
     */





    public function pushSellProgress($sold) {
        $metrics = array();
        $metrics[] = ['SellProgress', $sold, $this->date, null, self::CURRENCY_UNIT];
        return parent::insert($metrics);
    }

    /**
     * @zmajeric ~ 04/07/2018
     *
     * Pushes product units sold statistics.
     *
     * @return array
     */
    public function pushProductsUnitsSold() {
        $products = $this->retriever->getFakeProducts(20);
//        $dummyValues = $this->retriever->getFakeNumbers(20);

        $metrics = array();
        foreach ($products as $id => $product) {
            // TODO: change rand to actual values from API
            $sold = rand(0, 999);
            $metrics[] = ['ProductsSold', $sold, $this->date,
                ['channel' => $product->productName], self::CURRENCY_UNIT];
            $this->pushSellProgress($sold);
        }
        return parent::insert($metrics);
    }

    function pushAll() {
        $response = array();
        $response[] = $this->pushSellersOfTheDay();
        $response[] = $this->pushProductsUnitsSold();
        return $response;
    }
}