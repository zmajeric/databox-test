<?php
/**
 * Created by PhpStorm.
 * User: zmajeric
 * Date: 02/07/2018
 * Time: 18:48
 */

namespace App\Services;

class YoutubePusher extends Pusher {

    const DATABOX_YT_TOKEN = 'hy3i22ixenmsuoda3fs7xp';

    private $retriever;
    private $databoxClient;

    public function __construct() {
        parent::__construct(self::DATABOX_YT_TOKEN);
        $this->retriever = new YoutubeRetriever();
    }

    /**
     * @zmajeric ~ 03/07/2018
     *
     * Pushes general statistics data from channel.
     *
     * @return array
     */
    public function pushGeneralData() {
        $channelStats = $this->retriever->getChannelStats();
        $metrics = array();
        $metrics[] = ['SubscribersCount', $channelStats['subscriberCount'], now()->toDateTimeString(), null];
        $metrics[] = ['VideoCount', $channelStats['videoCount'], now()->toDateTimeString(), null];
        $metrics[] = ['ViewCount', $channelStats['viewCount'], now()->toDateTimeString(), null];

        return parent::insert($metrics);
    }

    /**
     * @zmajeric ~ 03/07/2018
     *
     * Pushes video statistics of most viewed videos. First 5 by default.
     *
     * @return array
     */
    public function pushMostViewed() {
        $mostViewed = $this->retriever->getMostViewed();
        $metrics = array();
        foreach ($mostViewed as $video) {
            $metrics[] = ['MostViewed', $video['statistics']['viewCount'], now()->toDateTimeString(), [
                'channel' => $video['snippet']['title']
            ]];
        }
        return parent::insert($metrics);
    }

    public function pushAll() : array{
        $response = array();
        $response[] = $this->pushGeneralData();
        $response[] = $this->pushMostViewed();
        return $response;
    }
}