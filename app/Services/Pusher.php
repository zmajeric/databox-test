<?php
/**
 * Created by PhpStorm.
 * User: zmajeric
 * Date: 04/07/2018
 * Time: 20:58
 */

namespace App\Services;


use Databox\Client;

abstract class Pusher {

    private $databoxClient;

    public function __construct($token) {
        $this->databoxClient = new Client($token);
    }

    function insert($metrics): array{
        $response = $this->databoxClient->insertAll($metrics);
        if ($response) return array("status" => "success", "push_token" => $response);
        else return array("status" => "error");
    }

    abstract function pushAll();
}