<?php
/**
 * Created by PhpStorm.
 * User: zmajeric
 * Date: 02/07/2018
 * Time: 18:57
 */

namespace App\Services;

use Google_Service_YouTube_ChannelStatistics as ChannelStats;
use Illuminate\Support\Facades\App;

class YoutubeRetriever {

    const DEFAULT_CHANNEL_ID = 'UC7_gcs09iThXybpVgjHZ_7g';

    private $client;
    private $channelId;

    public function __construct() {
        $this->client = App::make('Google_Service_Youtube');
        $this->channelId = self::DEFAULT_CHANNEL_ID;
    }

    /**
     * @zmajeric ~ 03/07/2018
     *
     * Returns general statistic for channel.
     *
     * @return ChannelStats
     */
    public function getChannelStats(): ChannelStats {
        $params = array('id' => $this->channelId);
        $part = 'contentDetails,statistics';

        $list = $this->client->channels->listChannels($part, $params);
        return $list[0]['statistics'];
    }

    /**
     * @zmajeric ~ 03/07/2018
     *
     * Returns array of most viewed videos in descending order.
     *
     * @param int $numberOfVideos to return. Default: first 5;
     * @return array
     */
    public function getMostViewed($numberOfVideos = 6) : array {
        $params['channelId'] = $this->channelId;
        $params['order'] = 'viewCount';
        $params['maxResults'] = $numberOfVideos;
        $part = 'snippet';

        $list = $this->client->search->listSearch($part, $params);

        $videosIds = array();
        foreach ($list->getItems() as $item) {
            if ($item['id']['videoId'])
                $videosIds[] = $item['id']['videoId'];
        }

        $part = 'snippet,statistics';
        $params = array('id' => implode(',', $videosIds));
        $videoList = $this->getVideos($part, $params);

        return $videoList;
    }

    /**
     * @zmajeric ~ 04/07/2018
     *
     * Return statistics of video(s), based on parameter 'params'
     *
     * @param string $part
     * @param $params : array("id"=>"<video_id_seperated_with_commas>")
     * @return mixed
     */
    public function getVideos($part = 'snippet,statistics', $params) {
        $videos = $this->client->videos->listVideos($part,$params);
        return $videos->getItems();
    }
    
}