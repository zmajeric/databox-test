<?php

namespace App\Console\Commands;

use App\Http\Controllers\MainController;
use App\Services\FakeJsonPusher;
use App\Services\YoutubePusher;
use Illuminate\Console\Command;

class SendMetrics extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:metrics {providers?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Forwards metrics from providers to Databox. Parameters: {providers} (optional: market | youtube )';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        if ($this->argument('providers')){
            // Should be more abstract :)
            $providers = explode(',',$this->argument('providers'));
            if (in_array('market',$providers)){
                $fakeJsonPusher = new FakeJsonPusher();
                $this->info(json_encode($fakeJsonPusher->pushAll()));
            }else if (in_array('youtube',$providers)){
                $youtubePusher = new YoutubePusher();
                $this->info(json_encode($youtubePusher->pushAll()));
            }
        }else{
            $fakeJsonPusher = new FakeJsonPusher();
            $this->info(json_encode($fakeJsonPusher->pushAll()));
            $youtubePusher = new YoutubePusher();
            $this->info(json_encode($youtubePusher->pushAll()));
        }
//        MainController::Send2Databox($this->argument('metric'),$this->argument('data'));
//        MainController::GetServiceData();
    }
}
