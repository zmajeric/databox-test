<?php
/**
 * Created by PhpStorm.
 * User: zmajeric
 * Date: 28/06/2018
 * Time: 19:00
 */

Route::get('/forward/all', 'ForwardController@all');
Route::get('/forward/fakedata/pushSellersOfTheDay', 'ForwardController@pushSellersOfTheDay');
Route::get('/forward/fakedata/pushSellProgress/{units}', 'ForwardController@pushSellProgress')
    ->middleware('is.number');
Route::get('/forward/fakedata/pushProductsUnitsSold', 'ForwardController@pushProductsUnitsSold');
Route::get('/forward/youtube/pushGeneralData/', 'ForwardController@pushGeneralData');
Route::get('/forward/youtube/pushMostViewed/', 'ForwardController@pushMostViewed');