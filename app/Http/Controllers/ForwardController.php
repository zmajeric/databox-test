<?php

namespace App\Http\Controllers;

use App\Services\FakeJsonPusher;
use App\Services\YoutubePusher;

class ForwardController extends Controller {

    public function __construct() {
    }

    public function all(FakeJsonPusher $fakeJsonPusher, YoutubePusher $youtubePusher) {
        $response = array();
        $response[] = $fakeJsonPusher->pushAll();
        $response[] = $youtubePusher->pushAll();
        return $this->response($response);
    }

    public function pushMarketData(FakeJsonPusher $fakeJsonPusher) {
        return $fakeJsonPusher->pushAll();
    }

    public function pushYoutubeData(YoutubePusher $youtubePusher) {
        return $youtubePusher->pushAll();
    }

    public function pushSellersOfTheDay(FakeJsonPusher $pusher) {
        $res = $pusher->pushSellersOfTheDay();
        return $this->response($res);
    }

    public function pushProductsUnitsSold(FakeJsonPusher $pusher) {
        $res = $pusher->pushProductsUnitsSold();
        return $this->response($res);
    }

    public function pushSellProgress($units, FakeJsonPusher $pusher) {
        $res = $pusher->pushSellProgress($units);
        return $this->response($res);
    }

    public function pushGeneralData(YoutubePusher $pusher) {
        $res = $pusher->pushGeneralData();
        return $this->response($res);
    }

    public function pushMostViewed(YoutubePusher $pusher) {
        $res = $pusher->pushMostViewed();
        return $this->response($res);
    }

    private function response($response) {
        return response($response, 200)
            ->header('Content-Type', 'application/json');
    }

}
