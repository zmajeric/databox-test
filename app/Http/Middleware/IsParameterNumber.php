<?php

namespace App\Http\Middleware;

use Closure;

class IsParameterNumber
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!is_numeric($request->route()->parameter('units')))
            return response(array("status"=>"error","description"=>"Parameter must be a value"));
        return $next($request);
    }
}
